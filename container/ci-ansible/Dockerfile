#syntax=docker/dockerfile:1.6
################################################################
#
FROM ubuntu:22.04
MAINTAINER HXR <code@hxr.io>

ARG CI=false
ARG CI_COMMIT_SHA=undefined
ARG CONTAINER_IMAGE=ci-ansible
ARG CONTAINER_VERSION=unknown
ARG METADATA=/var/opt/container/metadata

ENV DEBIAN_FRONTEND=noninteractive
ENV ANSIBLE_WHEEL_URL=https://files.pythonhosted.org/packages/6a/46/f3a297c7b316f7bcdbd9d82162440f316182215f4071c61d4d21dd158940/ansible_core-2.15.2-py3-none-any.whl
ENV ANSIBLE_SHA256SUM=07a8f0476904876aaaf51401c617519cdd9cfb6e1e3ac46597c35699b0b83d52

RUN apt-get update && \
    apt-get install --assume-yes --no-install-recommends coreutils python3-pip python3-wheel wget

RUN wget "${ANSIBLE_WHEEL_URL}" \
    && export ANSIBLE_WHEEL="$(basename ${ANSIBLE_WHEEL_URL})" \
    && printf "%s  %s\n" "${ANSIBLE_SHA256SUM}" "${ANSIBLE_WHEEL}" >> "${ANSIBLE_WHEEL}.sha256" \
    && printf "SHA256 checksum\n  %s\n" "$(sha256sum -c "${ANSIBLE_WHEEL}.sha256")" \
    && python3 -m pip install --user "${ANSIBLE_WHEEL}" \
    && rm "${ANSIBLE_WHEEL}"

RUN mkdir -p "$(dirname "${METADATA}")" \
    && echo "Writing container metadata to ${METADATA}" \
    && printf "CI=%s\n" "${CI}" >> "${METADATA}" \
    && printf "CI_COMMIT_SHA=%s\n" "${CI_COMMIT_SHA}" >> "${METADATA}" \
    && printf "CONTAINER_IMAGE=%s\n" "${CONTAINER_IMAGE}" >> "${METADATA}" \
    && printf "CONTAINER_VERSION=%s\n" "${CONTAINER_VERSION}" >> "${METADATA}" \
    && cat "${METADATA}"
