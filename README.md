# gitlab-ci-operator
Operate GitLab CI infrastructure

## Roadmap
- [ ] Debug and integration network
  - [ ] Connect developer tools into CI jobs
  - [ ] Connect CI to test deployment network
- [ ] Local package mirrors for runners
  - [ ] APT mirror
  - [ ] Python mirror
